#!/usr/bin/env python3
'''
Usage: Takes an activity id and reconstructs that activity based on the values stored in Aurora 

Todo: Replace all hard coded variables with enviroment variables / arguments etc
You need to set the following enviroment variables

A1AS_QUEUE_NAME
A1AS_DATABASE_HOST 
A1AS_DATABASE_USER 
A1AS_DATABASE_PASSWORD
A1AS_DATABASE_NAME
'''

#Required modules
import boto3
import json
import mysql.connector  #MySql official python connector module 
import time
import os
import sys
import code
import itertools

A1AS_QUEUE_NAME = os.environ.get('A1AA_QUEUE_NAME','dev-admiralone-data-command-queue.fifo')  
A1AS_DATABASE_HOST = os.environ.get('A1AS_DATABASE_HOST','localhost')
A1AS_DATABASE_USER = os.environ.get('A1AS_DATABASE_USER','admiral') 
A1AS_DATABASE_PASSWORD = os.environ.get('A1AS_DATABASE_PASSWORD',False)
A1AS_DATABASE_NAME = os.environ.get("A1AS_DATABASE_NAME","admiralone")


if not A1AS_DATABASE_PASSWORD:
    print("Need to specify database password as enviroment variable")
    exit(0)
if len(sys.argv) < 2:
    print("Not enough arguments, specify an ActivityId")
    exit(0)

# Choosing the resource from boto3 module
sqs = boto3.resource('sqs')

last_time = False
def time_passed():
    current_time = time.perf_counter()
    global last_time
    delta = current_time - last_time
    last_time = current_time
    return delta


def main():
    # Get the queue 
    queue = sqs.get_queue_by_name(QueueName=A1AS_QUEUE_NAME)

    #Connect to the database 
    connection = mysql.connector.connect(user=A1AS_DATABASE_USER, password=A1AS_DATABASE_PASSWORD,
            host=A1AS_DATABASE_HOST ,database=A1AS_DATABASE_NAME)


    processed = 0 #Keep track of how many messages the script has processed 
    last_time = time.perf_counter()
    debug_message = lambda msg_id,msg: "{0} | {1} | {2} (took {3} seconds)".format(processed, msg_id, msg, time_passed())

    activity_id = str(sys.argv[1])

    cursor = connection.cursor()
    query = ("SELECT item_name,item_value FROM activity_items WHERE activity = %s")
    cursor.execute(query,(activity_id,))
    activity = {}
    for result in cursor:
       activity[result[0]] = result[1].decode('UTF-8')
       
    #code.interact(local=locals())
    #TODO: read cursor results
    #TODO: expand into json        
    #TODO: return
    #TODO: create some kind of api that can be wrapped in rest later etc

    print(json.dumps(activity))
main()
