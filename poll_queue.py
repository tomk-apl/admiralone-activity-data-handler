#!/usr/bin/env python3
'''
Usage: Takes items from the activity stream queue and upserts them into aurora

Todo: Replace all hard coded variables with enviroment variables / arguments etc
You need to set the following enviroment variables

A1AS_QUEUE_NAME
A1AS_DATABASE_HOST 
A1AS_DATABASE_USER 
A1AS_DATABASE_PASSWORD
A1AS_DATABASE_NAME
'''

#Required modules
import boto3
import json
import mysql.connector  #MySql official python connector module 
import time
import os

A1AS_QUEUE_NAME = os.environ.get('A1AA_QUEUE_NAME','dev-admiralone-data-command-queue.fifo')  # TODO: Replace with enviroment variable
A1AS_DATABASE_HOST = os.environ.get('A1AS_DATABASE_HOST','localhost')
A1AS_DATABASE_USER = os.environ.get('A1AS_DATABASE_USER','admiral') 
A1AS_DATABASE_PASSWORD = os.environ.get('A1AS_DATABASE_PASSWORD',False)
A1AS_DATABASE_NAME = os.environ.get("A1AS_DATABASE_NAME","admiralone")

if not A1AS_DATABASE_PASSWORD:
    print("Need to specify database password as enviroment variable")
    exit(0)

# Choosing the resource from boto3 module
sqs = boto3.resource('sqs')

last_time = False

def time_passed():
    current_time = time.perf_counter()
    global last_time
    delta = current_time - last_time
    last_time = current_time
    return delta 

def validate_message(message_body):
    '''Check if a message (dictionary) has the proper structure - a session id and an activity ID'''
    
    has_method = ('method' in message_body)
    has_keys = ('keys' in message_body)

    if has_keys:
        session_id_valid = ('SessionId' in message_body['keys']) and ('S' in message_body['keys']['SessionId'])
        activity_id_valid = ('ActivityId' in message_body['keys']) and ('S' in message_body['keys']['ActivityId']) 
        if not (session_id_valid and activity_id_valid):
            return False
    if has_method:
        if message_body['method'] == 'INSERT':
            has_values = ('values' in message_body)
            if not has_values:
                return False
        elif message_body['method'] == 'MODIFY':
            has_delta = ('delta' in message_body)
            if not has_delta:
                return False
    return True 

def main():
    # Get the queue 
    queue = sqs.get_queue_by_name(QueueName=A1AS_QUEUE_NAME)

    #Connect to the database 
    connection = mysql.connector.connect(user=A1AS_DATABASE_USER, password=A1AS_DATABASE_PASSWORD,
                                          host=A1AS_DATABASE_HOST ,database=A1AS_DATABASE_NAME)


    processed = 0 #Keep track of how many messages the script has processed 
    last_time = time.perf_counter() 
    
    debug_message = lambda msg_id,msg: "{0} | {1} | {2} (took {3} seconds)".format(processed, msg_id, msg, time_passed())

    # Start processing messages from the Amazon SQS Queue
    print(str(time_passed()) + " | Going to start receiving messages for queue")
    while True:
        for message in queue.receive_messages():
                processed += 1

                message_body = json.loads(message.body)
                is_valid_message = validate_message(message_body)
                if not is_valid_message:
                    print(debug_message(message.message_id, "Is invalid and will be deleted from the queue"))
                    print("Invalid message body: " + message.body)
                    message.delete()
                    continue

                message_method = message_body["method"]
                if message_method == "INSERT":
                    cursor = connection.cursor()
                    session_query = ("CALL upsert_session(%s,%s);")
                    session_id = message_body["keys"]["SessionId"]["S"]
                    activity_id = message_body["keys"]["ActivityId"]["S"]
                    cursor.execute(session_query, (session_id, activity_id))
                    cursor.close()

                    cursor = connection.cursor()
                    activity_query = ("CALL upsert_activitys(%s,%s,%s);")
                    activity_id = message_body["keys"]["ActivityId"]["S"]
                    channel_code = message_body["values"]["ChannelCode"]
                    activity_code = message_body["values"]["ActivityCode"]
                    cursor.execute(activity_query, (activity_id, activity_code, channel_code))
                    cursor.close()

                    print(debug_message(message.message_id, "Upserted new {2} {3} activity & session [AID: {0} | SID:{1}]".format(activity_id,session_id,activity_code,channel_code)))
                    message.delete()

                elif message_method == "MODIFY":
                    
                    cursor = connection.cursor()
                    delta_query = ("CALL upsert_activity_item(%s,%s,%s);")
                    activity_id = message_body["keys"]["ActivityId"]["S"]
                    item_name = list(message_body["delta"])[0]
                    item_value = message_body["delta"][item_name]
                    cursor.execute(delta_query,(activity_id,item_name,item_value))
                    #connection.free_result()
                    cursor.close() 
                    connection.commit()
                    print(debug_message(message.message_id, "Updated key '{2}' in activity  [AID:{1} | SID:{0}]".format(session_id,activity_id,item_name)))
                    message.delete()


    connection.close()
main()



#delta_query_a = ('''
#                     SET 
#                       @activity = %s, 
#                       @item_name = %s,
#                       @item_value  = %s;
#                       ''')
#                   delta_query_b = ('''
#                   INSERT INTO activity_items 
#                       (activity, item_name, item_value)
#                       VALUES
#                       (@activity,@item_name,@item_value)
#                   ON DUPLICATE KEY UPDATE
#                       item_value = @item_value;
#                   ''')
